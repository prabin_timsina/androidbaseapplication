package prabin.timsina.baseproject

import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object SingletonObjectFactory {
    private val okHttpClient: OkHttpClient by lazy {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC)
        OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()
    }

    val jsonPlaceHolderApi: TodoModel.JsonPlaceHolderService by lazy {
        val retrofit = Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .build()
        retrofit.create(TodoModel.JsonPlaceHolderService::class.java)
    }

    val gson: Gson by lazy {
        Gson()
    }
}