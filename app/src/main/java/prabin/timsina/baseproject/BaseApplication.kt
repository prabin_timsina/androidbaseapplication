package prabin.timsina.baseproject

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.squareup.leakcanary.LeakCanary
import io.fabric.sdk.android.Fabric
import prabin.timsina.debugging.CrashReportingTree
import prabin.timsina.debugging.CustomDebugTree
import timber.log.Timber

class BaseApplication : Application() {
    @Database(entities = [TodoModel.Todo::class], version = 1)
    abstract class AppDatabase : RoomDatabase() {
        abstract fun todoDao(): TodoModel.TodoDao
    }

    override fun onCreate() {
        instance = this

        super.onCreate()

        initLogger()
        initFabric()
        initLeakDetector()
    }

    private fun initLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(CustomDebugTree())
        } else {
            Timber.plant(CrashReportingTree())
        }
    }

    private fun initFabric() {
        Fabric.with(
            this,
            Crashlytics.Builder()
                .core(CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build())
                .build()
        )
    }

    private fun initLeakDetector() {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return
        }
        LeakCanary.install(this)
    }

    companion object {
        lateinit var instance: BaseApplication

        val db: AppDatabase by lazy {
            Room.databaseBuilder(instance, AppDatabase::class.java, "base-application").build()
        }
    }
}
