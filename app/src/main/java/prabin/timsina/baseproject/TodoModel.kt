package prabin.timsina.baseproject

import androidx.room.*
import retrofit2.http.GET
import retrofit2.http.Path

object TodoModel {
    @Entity
    data class Todo(
        @PrimaryKey val id: Int,
        @ColumnInfo(name = "userId") val userId: Int,
        @ColumnInfo(name = "title") val title: String,
        @ColumnInfo(name = "completed") val completed: Boolean
    )

    @Dao
    interface TodoDao {
        @Insert(onConflict = OnConflictStrategy.REPLACE)
        suspend fun insert(todo: Todo)

        @Query("SELECT * FROM todo WHERE id = (:id)")
        suspend fun get(id: Int): Todo
    }

    interface JsonPlaceHolderService {
        @GET("/todos/{id}")
        suspend fun getTodo(@Path("id") id: Int): TodoModel.Todo
    }
}
