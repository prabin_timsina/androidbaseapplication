package prabin.timsina.baseproject

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import prabin.timsina.baseproject.SingletonObjectFactory.jsonPlaceHolderApi
import timber.log.Timber

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        lifecycleScope.launch(CoroutineExceptionHandler { _, exception ->
            Timber.e(exception)
        }) {
            fetchAndSave()
        }
    }

    private suspend fun fetchAndSave() {
        withContext(Dispatchers.IO) {
            try {
                // Fetch data from internet
                val networkResult = jsonPlaceHolderApi.getTodo(1)
                Timber.d("Successfully fetched todo: $networkResult")

                // Insert this data to database
                BaseApplication.db.todoDao().insert(networkResult)

                // Get the data from database
                val dbResult = BaseApplication.db.todoDao().get(networkResult.id)

                // Verify database result is same as network result
                if (dbResult == networkResult) {
                    Timber.d("Save and get successful to db")
                } else {
                    Timber.e("Db result does not match with network result")
                }

            } catch (e: Exception) {
                Timber.e(e, "Failed to fetch todo")
            }
        }
    }
}
