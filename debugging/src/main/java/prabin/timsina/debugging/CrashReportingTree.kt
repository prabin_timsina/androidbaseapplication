package prabin.timsina.debugging

import android.util.Log
import com.crashlytics.android.Crashlytics
import timber.log.Timber

class CrashReportingTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG) {
            return
        }
        if (priority == Log.ERROR) {
            Crashlytics.logException(t)
        }
        Crashlytics.log(priority, tag, message)
    }
}
